use std::io;
use std::io::Write;
use std::process::exit;

fn main() {
    println!("*** Welcome to the babelfish translator ***");
    loop {
        print!("Enter the text you want to translate (type 'exit' to quit the application): ");
        let input_text = get_input();
        if input_text == String::from("exit") {
            exit(0);
        }

        const MENU: &str = "How do you want to translate the text:
        1. Translate to leet speak
        2. Translate to half speak
        3. Translate to leet Yoda speak
        4. Translate to leet Yodadada speak";
        println!("{}", MENU);
        print!("Please enter your selection: ");

        let mut output = String::from("");
        while output.is_empty() {
            output = match get_input().as_str() {
                "1" => translate_leet_speak(&input_text),
                "2" => translate_half_speak(&input_text),
                "3" => translate_yoda_speak_once(&input_text),
                "4" => translate_yoda_speak(&input_text, 3),
                _ => {
                    print!("Please enter a valid option (1, 2, 3, 4): ");
                    String::from("")
                }
            }
        }
        println!("The text has been translated to: {}\n", output);
    }
}

pub fn get_input() -> String {
    match io::stdout().flush() {
        Ok(()) => {}
        Err(error) => println!("error: {}", error),
    }
    let mut input = String::new();
    match io::stdin().read_line(&mut input) {
        Ok(_n) => {}
        Err(error) => println!("error: {}", error),
    }
    input.trim().to_string()
}

pub fn translate_leet_speak(input: &String) -> String {
    let output = input
        .replace("elite", "leet")
        .replace("hacker", "haxor")
        .replace("a", "4")
        .replace("e", "3")
        .replace("o", "0")
        .replace("i", "1");
    output
}

pub fn translate_half_speak(input: &String) -> String {
    let input_length = input.chars().count();
    format!("{}...", input[..(input_length/2)].to_string())
}

pub fn translate_yoda_speak(input: &String, times: usize) -> String {
    let mut output = input.clone();
    for _ in 0..times {
        output = match output.rfind(" ") {
            Some(index) => format!("{} {}", output[index+1..].to_string(), output[..index].to_string()),
            None => output
        };
    }
    translate_leet_speak(&output)
}

pub fn translate_yoda_speak_once(input: &String) -> String {
    translate_yoda_speak(input, 1)
}
